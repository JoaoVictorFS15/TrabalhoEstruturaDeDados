#include <iostream>

using namespace std;

const int tamanho = 5;
int selecao;

int vetor[tamanho];

void inserirNumeros(int vet[], int tam){
    for(int i=0; i < tam; i++){
        cout << "Posicao " << (i+1) << ": ";
        cin >> vet[i];
    }
}

void ordenarNumeros(int vet[], int tam){
	cout << "Escolha uma forma de ordenar os valores inseridos: 1 - Selection Sort; 2 - Insertion Sort; 3 - Bubble Sort: " ;
	cin >> selecao;
		if(selecao==1){
			for(int i = 0; i < tam; i++){
				int menor = i;
				for(int j =i+1; j < tam; j++){
					if(vet[j] < vet[menor]){
						menor = j;
					}
				}
				if (i != menor){
					int temp = vet[i];
					vet[i] = vet[menor];
					vet[menor] = temp;
				}
			}
        cout << "A posicao do vetor ordenado pelo selection sort e:" << endl;
        for(int i =0; i < tam; i++){
		cout << "Posicao " << (i+1) << ": " << vet[i] << endl;
        }
}

        if(selecao==2){
            for (int i=1;i<tamanho;i++){
                int comp = vetor[i];
                int j = i - 1;
                    for (;j>=0 && comp < vetor[j];j--){
                        vetor[j+1]=vetor[j];
                    }
                vetor[j+1] = comp;
            }
        cout << "A posicao do vetor ordenado pelo insertion sort e:" << endl;
        for(int i =0; i < tam; i++){
		cout << "Posicao " << (i+1) << ": " << vet[i] << endl;
        }
        }
        if(selecao==3){
            for(int i=0; i < tam; i++){
                for(int j = tam-1; j >= 1 ;j--){
                    if(vet[j-1] > vet[j]){
                        int temp = vet[j-1];
                        vet[j-1] = vet[j];
                        vet[j] = temp;
                    }
                }
            }
        cout << "A posicao do vetor ordenado pelo bublle sort e:" << endl;
        for(int i =0; i < tam; i++){
		cout << "Posicao " << (i+1) << ": " << vet[i] << endl;
        }
   	}

}

int main()
{
    inserirNumeros(vetor, tamanho);
    ordenarNumeros(vetor, tamanho);
    while(selecao>3){
        cout<< "Opcao invalida digite um numero de 1 a 3."<<endl;
        ordenarNumeros(vetor,tamanho);
    }

    return 0;
}
