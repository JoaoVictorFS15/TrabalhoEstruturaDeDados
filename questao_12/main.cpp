#include <iostream>

using namespace std;

const int tamanho = 10;

typedef struct {
	int topo;
	int item[tamanho];
} PILHA;

void inicializarPilha (PILHA *p) {
	p->topo = -1 ;
}

int pilhaVazia(PILHA *p) {
  if(p->topo == -1)
     return true;
  else
     return false;
}

int pilhaCheia(PILHA *p) {
	if (p->topo == tamanho-1)
		return true;
	else
		return false;
}

void empilhar(PILHA *p, int x) {
	p->item[++p->topo] = x;
}

int desempilhar(PILHA *p) {
	return (p->item[p->topo--]);
}

int topoPilha(PILHA *p) {
	if(pilhaVazia(p) == 1)
		return 0;
	else
		return (p->item[p->topo]);
}


int main() {

	int numero[tamanho];
	PILHA p;

	inicializarPilha(&p);

	cout << "DIGITE " << tamanho << " NUMEROS:\n";

	for( int i=0 ; i < tamanho ; i++){
		cout << "NUMERO " << i+1 << ": ";
		cin >> numero[i];

		empilhar(&p, numero[i]);
	}

	for(int i=0; !pilhaVazia(&p); i++) {
		cout << desempilhar(&p);
	}

	cout <<"\n";
	return 0;
}
